\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Protostar}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Telescopes}{4}{0}{1}
\beamer@sectionintoc {2}{Observation}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{Geometry of protostar}{5}{0}{2}
\beamer@subsectionintoc {2}{2}{Toy model}{6}{0}{2}
\beamer@subsectionintoc {2}{3}{Cyclic-\ce {C3H2} envelope}{7}{0}{2}
\beamer@subsectionintoc {2}{4}{\ce {SO}}{10}{0}{2}
\beamer@sectionintoc {3}{Interpretation}{11}{0}{3}
\beamer@sectionintoc {4}{Summary}{13}{0}{4}
\beamer@sectionintoc {5}{Reference}{14}{0}{5}
